﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] private GameObject _deathFX;
    [SerializeField] private Transform _parent;
    [SerializeField] private int _scorePerHit = 12;
    [SerializeField] private int _hits = 3;

    private ScoreBoard _scoreBoard;


    private void Start()
    {
        AddBoxCollider();
        _scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void AddBoxCollider()
    {
        var boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
    }

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();

        if (_hits <= 0)
        {
            KillEnemy();
        }
    }

    private void ProcessHit()
    {
        _scoreBoard.ScoreHit(_scorePerHit);
        _hits--;
    }

    private void KillEnemy()
    {
        var go = Instantiate(_deathFX, transform.position, Quaternion.identity);
        go.transform.parent = _parent;
        Destroy(gameObject);
    }
}
