﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour {

    [Tooltip("In seconds")][SerializeField] private float _levelLoadDelay;

    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
        Invoke("ReloadScene", _levelLoadDelay);
    }

    private void StartDeathSequence()
    {
        print("Player Dying");
        SendMessage("OnPlayerDeath");
    }

    void ReloadScene()
    {
        SceneManager.LoadScene(1);
    }
}
