﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    [Header("General")]
    [Tooltip("In ms^-1")] [SerializeField] private float _xSpeed = 15f;
    [Tooltip("In ms^-1")] [SerializeField] private float _ySpeed = 15f;
    [Tooltip("In m")] [SerializeField] private float _xRange = 5f;
    [Tooltip("In m")] [SerializeField] private float _yMin = -3.5f;
    [Tooltip("In m")] [SerializeField] private float _yMax = 3.2f;

    [SerializeField] private ParticleSystem[] _guns;

    [Header("Player input")]
    [SerializeField] private float _positionPitchFactor = -8f;
    [SerializeField] private float _controlPitchFactor = -20f;
    [SerializeField] private float _positionYawFactor = 8f;
    [SerializeField] private float _controlRollFactor = -20;

    [Header("Effects")]
    [SerializeField] private GameObject _deathFx;

    private float _xThrow, _yThrow;
    private bool _isControlEnabled;
    // Use this for initialization
    void Start () {
        _isControlEnabled = true;
    }
    
    // Update is called once per frame
    void Update ()
    {
        if (_isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    private void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateGuns(true);
        }
        else
        {
            ActivateGuns(false);
        }
    }

    private void ActivateGuns(bool active)
    {
        foreach(var gun in _guns)
        {
            var emission = gun.emission;
            emission.enabled = active;
        }
    }

    private void ProcessRotation()
    {
        var pitchDueToPosition = transform.localPosition.y * _positionPitchFactor;
        var pitchDueToControlThrow = _yThrow * _controlPitchFactor;
        var pitch = pitchDueToPosition + pitchDueToControlThrow;

        var yaw = transform.localPosition.x * _positionYawFactor;

        var roll = _xThrow * _controlRollFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessTranslation()
    {
        _xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        _yThrow = CrossPlatformInputManager.GetAxis("Vertical");

        var xOffset = _xThrow * _xSpeed * Time.deltaTime;
        var yOffset = _yThrow * _ySpeed * Time.deltaTime;

        var newX = Mathf.Clamp(transform.localPosition.x + xOffset, -_xRange, _xRange);
        var newY = Mathf.Clamp(transform.localPosition.y + yOffset, _yMin, _yMax);

        transform.localPosition = new Vector3(newX, newY, transform.localPosition.z);
    }

    void OnPlayerDeath() //called by string reference
    {
        print("Control frozen");
        _isControlEnabled = false;
        _deathFx.SetActive(true);
    }
}
